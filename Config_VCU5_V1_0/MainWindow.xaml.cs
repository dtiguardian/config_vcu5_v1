﻿/// Starting Date: December 10th, 2016
/// By: Brad Chrabaszcz
/// For: Diesel Tech Industries
/// Reason: Developing a new Config program to interact with the latest VCU 5.0, 
/// updating the user interface and functionality
/// 


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Config_VCU5_V1_0
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.Width = 1365;
            this.Height = this.Width * 0.818181818;
        }

        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);
            HwndSource source = HwndSource.FromVisual(this) as HwndSource;
            if (source != null)
            {
                source.AddHook(new HwndSourceHook(WinProc));
            }
        }
        public const Int32 WM_EXITSIZEMOVE = 0x0232;
        private IntPtr WinProc(IntPtr hwnd, Int32 msg, IntPtr wParam, IntPtr lParam, ref Boolean handled)
        {
            IntPtr result = IntPtr.Zero;
            switch (msg)
            {
                case WM_EXITSIZEMOVE:
                    {
                        this.Height = this.Width * 0.666;
                        break;
                    }
            }

            return result;
        }
        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            if (sizeInfo.HeightChanged)
            {
                this.Height = sizeInfo.NewSize.Width * 0.666;
            }
            else
            {
                this.Width = sizeInfo.NewSize.Height * 1.502;
            }
        }

        private void ValveImage_Click(object sender, RoutedEventArgs e)
        {
            Style redStyle   =  this.FindResource("closedValveButton")   as Style;
            Style greenStyle =  this.FindResource("openValveButton") as Style;
            Style yellowStyle = this.FindResource("movingValveButton") as Style;
            Style naStyle =     this.FindResource("noValveButton") as Style;
            
            BitmapImage imgSourceClose = new BitmapImage(new Uri("D:/Config Development/Config_VCU5_V1_0/Config_VCU5_V1_0/Config_VCU5_V1_0/Resources/newvalveclosed2.png"));
            BitmapImage imgSourceOpen = new BitmapImage(new Uri("D:/Config Development/Config_VCU5_V1_0/Config_VCU5_V1_0/Config_VCU5_V1_0/Resources/newvalveopen2.png"));
            BitmapImage imgSourceMoving = new BitmapImage(new Uri("D:/Config Development/Config_VCU5_V1_0/Config_VCU5_V1_0/Config_VCU5_V1_0/Resources/newvalvemoving2.png"));

            if (ValveImage.Style == redStyle)
            {
                ValveImage.Style = greenStyle;
                valveImage.Source = imgSourceOpen;
                ValveStatus.Text = "Valve Status: Open";
            }
            else
            {
                ValveImage.Style = redStyle;
                valveImage.Source = imgSourceClose;
                ValveStatus.Text = "Valve Status: Closed";
            }
        }
    }
}
